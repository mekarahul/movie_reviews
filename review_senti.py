# -*- coding: utf-8 -*-
"""
Created on Mon Jan 08 10:23:20 2018

@author: RA388492
"""
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from nltk.tokenize import word_tokenize;
from nltk.corpus import stopwords
""" function for removing stopwords and numbers """
"""preparing the train data """
senti_dic = {}
movie_review=[]
for each_line in open('senti_dict.txt'):
    word,score = each_line.strip().split('\t')
    senti_dic[word] = int(score)
train=pd.read_csv("labeledTrainData.tsv",header=0,delimiter='\t',quoting=3)
num_reviews = train["review"].size 
k=[]
true=[]
stops=set(stopwords.words("english"))   
for x in xrange(0,num_reviews):
    clean_review=train["review"][x].lower()
    s1=BeautifulSoup(clean_review).get_text()
    token=word_tokenize(s1)
    mean=[w for w in token if not w in stops]
    j=sum( senti_dic.get(w, 0) for w in mean)
    k.append(j)
    if j<1:
        result=0
    else:
        result=1
    movie_review.append(result)
    true.append(train["sentiment"][x])
print accuracy_score(true,movie_review)*100
