# -*- coding: utf-8 -*-
"""
Created on Mon Jan 08 12:36:24 2018

@author: RA388492
"""
from bs4 import BeautifulSoup
import pandas as  pd
import nltk
import re
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
def review_to(raw_review):
    s1=BeautifulSoup(raw_review).get_text()
    letter_only=re.sub("[^a-zA-Z]"," ",s1)
    lower_case = letter_only.lower()        
    words = lower_case.split() 
    return(words)
def review_to_sentences( review, tokenizer):
    raw_sentences = tokenizer.tokenize(review.strip())
    sentences = []
    for raw_sentence in raw_sentences:
        # If a sentence is empty, skip it
        if len(raw_sentence) > 0:
            sentences.append( review_to( raw_sentence))
            # Otherwise, call review_to_wordlist to get a list of words    
    return sentences
    
train =pd.read_csv("labeledTrainData.tsv",header=0,delimiter="\t",quoting=3)
test =pd.read_csv("testData.tsv",header=0,delimiter="\t",quoting=3)
unlabeled_train =pd.read_csv("unlabeledTrainData.tsv",header=0,delimiter="\t",quoting=3)

print train["review"].size
print test["review"].size
print unlabeled_train["review"].size

sentences=[]

for review in train["review"]:
    sentences += review_to_sentences(review, tokenizer)

print "Parsing sentences from unlabeled set"
for review in unlabeled_train["review"]:
    sentences += review_to_sentences(review, tokenizer)