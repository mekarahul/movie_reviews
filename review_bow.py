# -*- coding: utf-8 -*-
"""
Created on Thu Jan 04 15:03:18 2018

@author: RA388492
"""
from bs4 import BeautifulSoup
import pandas as  pd
import re
from nltk.corpus import stopwords
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
""" function for removing stopwords and numbers """
def review_to(raw_review):
    s1=BeautifulSoup(raw_review).get_text()
    letter_only=re.sub("[^a-zA-Z]"," ",s1)
    lower_case = letter_only.lower()        
    words = lower_case.split() 
    stops=set(stopwords.words("english"))
    meaningful_words=[w for w in words if not w in stops]
    return( " ".join( meaningful_words ))
"""preparing the train data """
all_review=[] 
train=pd.read_csv("labeledTrainData.tsv",header=0,delimiter='\t',quoting=3)
num_reviews = train["review"].size 
for x in xrange(0,num_reviews):
    all_review.append(review_to(train["review"][x]))
vectorizer = CountVectorizer(analyzer = "word",   \
                             tokenizer = None,    \
                             preprocessor = None, \
                             stop_words = None,   \
                             max_features = 5000) 
train_features=vectorizer.fit_transform(all_review).toarray()
print train_features.shape  
forest = RandomForestClassifier(n_estimators = 100)
forest = forest.fit( train_features, train["sentiment"] )

"""test data """
test=pd.read_csv("testData.tsv",header=0,delimiter="\t",quoting=3)
test_size=test.size
print test.size
clean_test=[]
for x in xrange(0,test_size/2):
    clean_test.append(review_to(test["review"][x]))
test_features=vectorizer.fit_transform(clean_test).toarray()
result=forest.predict(test_features)
output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )
output.to_csv( "Bag_of_Words_model.csv", index=False, quoting=3 )